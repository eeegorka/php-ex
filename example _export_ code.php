<?php
public function sendFinanceStatistic(Request $request){
    $param = $request->all();
    $data= collect();
    $result = DB::table('HIDE TABLE NAME')->where('country_id',session('country_id'));
    if(!empty($param['all_fields'])){
        $result->select(
            'id',
            'model_year_id',
            'first_name',
            'last_name',
            'email',
            'phone',
            'urgency',
            'location',
            'contact_time',
            'finance',
            'monthly_income',
            'monthly_expenses',
            'vehicle_price',
            'deposit_amount',
            'interest_rate',
            'payment_period',
            'insurance',
            'insurance_provider',
            'expiry_month',
            'expiry_year',
            'trade_in',
            'created_at'
        );
    }else{
        $result->select('id','view_status','created_at');
    }

    if(!empty($param['start'])){
        $result->where('created_at','>=',convertFromAdminTimeToUTC($param['start']));
    }
    if(!empty($param['end'])){
        $result->where('created_at','<=', Carbon::parse(convertFromAdminTimeToUTC($param['end']))->addHours(24)->toDateTimeString());
    }

    $result->orderBy('id')->chunk(100,function ($finance_requests) use (&$data){
        $finance_requests->transform(function ($finance_request){
            $finance_request->created_at = Carbon::parse( $finance_request->created_at)->format('Y-m-d');
            return $finance_request;
        });
        $data->push($finance_requests);
    });

    $data = $data->collapse()->sortByDesc('created_at');

    if(!empty($param['all_fields'])){
        $sort_data = [
            '#',
            'Car name',
            'First Name',
            'Last Name',
            'Email',
            'Phone',
            'Urgency',
            'Location',
            'Contact time',
            'Finance',
            'Monthly Income',
            'Monthly Expenses',
            'Vehicle Price',
            'Deposit',
            'Interest Rate',
            'Payment Period',
            'Insurance' ,
            'insurance_provider',
            'Expiry month',
            'Expiry year',
            'Trade In',
            'Created Date',
            'Trade In Cars',
        ];
        $timezone = config("app.site_timezone");
        $data->transform(function ($item) use (&$sort_data, $timezone){

            $model_year = ModelYear::find($item->model_year_id);

            $model_year_name = $model_year->year.' '.$model_year->make->name.' '.$model_year->model->name;

            $location = State::find($item->location);
            $trade_in_cars = TradeInCar::where('finance_request_id',$item->id)->get();
            if(!empty($model_year_name)){
                $item->model_year_id = $model_year_name;
            }
            if(!empty($location)){
                $item->location = isset($location->name)? $location->name: '';
            }
            if(!empty($trade_in_cars)){
                $trade_in_cars->transform(function ($car){
                    $year = !empty($car->year)?$car->year:'';
                    $make = !empty($car->make)?$car->make:'';
                    $model = !empty($car->model)?$car->model:'';
                    $variant = !empty($car->variant)?$car->variant:'';
                    return $year.' '.$make.' '.$model.' '.$variant;
                });
                $item->trade_in_cars = $trade_in_cars->implode(', ');
            }
            $item->created_at = Carbon::parse($item->created_at)->setTimezone($timezone)->toDateTimeString();
            return collect($item)->values();
        });
        $sort_data = array_prepend($data->toArray(),$sort_data);
    }else{
        $data = $data->groupBy('created_at');
        $sort_data = [
            [
                'Date',
                'Unread requests count',
                'All requests count'
            ]
        ];
        $data->each(function ($fr_date,$time) use (&$sort_data){
            $count = $fr_date->count();
            $unread = $fr_date->where('view_status',false)->count();
            $sort_data[] = [$time,$unread,$count];
        });
    }
    Excel::create('EXPORT_FILE_' . Carbon::now()->format('Y_m_d_h_i_s'), function ($excel) use ($sort_data) {
        $excel->sheet('EXPORT_FILE_', function ($sheet) use ($sort_data) {
            $sheet->fromArray($sort_data);
            $sheet->row(2, function ($row) {
                $row->setFontWeight('bold');
            });
        });
    })->export('xlsx');

}