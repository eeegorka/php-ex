<?php


namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ModelYearView;
use App\Models\Setting;

class CalculatePopularityValue extends Command
{
    protected $signature = 'calculatePV';
    protected $description = 'Get max value for tag Popular for models';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $country_id = Setting::all();
        $country_id->each(function ($setting){
            $max_model_value = $this->getMaxValueModel($setting->country_id);
            $setting->model_views_count_value = $max_model_value;
            $res = $setting->save();
            $this->info("max model value: $max_model_value |------------------------------| country: ".$setting->country_id." result: ". $res);
        });

    }

    public function getMaxValueModel($user_country){

        $all_country_views = ModelYearView::where('country_id',$user_country)->get();
        $count_model_year = $all_country_views->groupBy('model_year_id')->map(function ($item,$key) use ($user_country){
            $this->info("model: $key |---------| country: $user_country |----------| count: ".$item->count());
            return $item->count();
        });

        return $count_model_year->max();
    }
}