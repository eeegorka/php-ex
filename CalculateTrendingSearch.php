<?php


namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\TrendingSearch;
use App\Models\Country;
use App\Models\Make;

class CalculateTrendingSearch extends Command
{
    protected $signature = 'CalcTrendingSearch';
    protected $description = 'Get popular searches';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $countries = Country::all();
        $countries->each(function ($country){
            $count_makes = $country->trending_settings->count_makes?$country->trending_settings->count_makes:3;
            $count_models = $country->trending_settings->count_models?$country->trending_settings->count_models:3;
            $this->info("calculate top makes for $country->name start... ");
            $this->calculateMakes($country,$count_makes);
            $this->info("...calculate top makes for $country->name end");
            $this->info("calculate top models for $country->name start... ");
            $this->calculateModels($country,$count_models);
            $this->info("...calculate top models for $country->name end");
        });
    }

    public function calculateMakes($country,$count_makes){
        $top_makes = collect();
        if($country->trending_search->isNotEmpty()){
            $this->line("start creating makes array for $country->name ...");
            TrendingSearch::where('country_id',$country->id)
                ->whereNotNull('make_id')
                ->chunk(500,function ($makes) use (&$top_makes){
                    if($makes->isNotEmpty()){
                        $makes->each(function ($make) use (&$top_makes){
                            $top_makes->push(collect($make)->only(['id','make_id','updated_at'])->toArray());
                        });
                    }
                });
            if($top_makes->isNotEmpty()){

                $this->line("sort started with ".$top_makes->count().' results');

                $sort_search = $top_makes->groupBy('make_id')->sortByDesc(function ($item){
                    return $item->count();
                });
                $sort_search->each(function ($item,$key) use ($country){
                    $make_for_view = Make::find($key)->make_country()->where('country_id',$country->id);
                    $make_for_view->update(['count_views'=>count($item)]);
                });
                $sort_search = $sort_search->slice(0,$count_makes)->keys()->toArray();
                $this->line("makes array is sorted and cropped");

                if(!empty($sort_search)){
                    $info_data = implode(',',$sort_search);
                    $country->trending_settings->trending_makes = $info_data;
                    $result = $country->trending_settings->save();

                    if($result){
                        $this->info("result for $country->name saved result >>>>> $info_data >>>> success");
                    }else{
                        $this->error("result for $country->name failed, result is missed");
                    }
                }
            }else{
                $this->line("makes array is empty");
            }
        }else{
            $this->line('#'.$country->id.' '.$country->name.' don`t have results');
        }
    }

    public function calculateModels($country,$count_models){
        $top_models = collect();
        if($country->trending_search->isNotEmpty()){
            $this->line("start creating models array for $country->name ...");
            TrendingSearch::where('country_id',$country->id)
                ->whereNotNull('model_id')
                ->chunk(500,function ($models) use (&$top_models){
                    if($models->isNotEmpty()){
                        $models->each(function ($model) use (&$top_models){
                            $top_models->push(collect($model)->only(['id','model_id','updated_at'])->toArray());
                        });
                    }
                });
            if($top_models->isNotEmpty()){

                $this->line("sort started with ".$top_models->count().' results');

                $sort_search = $top_models->groupBy('model_id')->sortByDesc(function ($item){
                    return $item->count();
                })->slice(0,$count_models)->keys()->toArray();

                $this->line("models array is sorted and cropped");

                if(!empty($sort_search)){
                    $info_data = implode(',',$sort_search);
                    $country->trending_settings->trending_models = $info_data;
                    $result = $country->trending_settings->save();

                    if($result){
                        $this->info("result for $country->name saved result >>>>> $info_data >>>> success");
                    }else{
                        $this->error("result for $country->name failed, result is missed");
                    }
                }
            }else{
                $this->line("models array is empty");
            }
        }else{
            $this->line('#'.$country->id.' '.$country->name.' don`t have results');
        }
    }
}